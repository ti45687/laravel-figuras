<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Figuras;
use App\Models\Marcas;
class FigurasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $figuras = Figuras::select('figuras.id','nombre','origen','categoria','escala','precio','fecha','id_marca','marca')->join('marcas','marcas.id','=','figuras.id_marca')->get();
        $marcas = Marcas::all();
        return view('figuras',compact('figuras','marcas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $figura = new Figuras($request->input());
        $request->validate([
            'nombre' => 'required|string|max:100',
            'origen' => 'required|string|max:50',
            'id_marca' => 'required|numeric',
            'categoria' => 'required|string|max:50',
            'escala' => 'required|string',
            'precio' => 'required|numeric|digits_between:1,10',
            'fecha' => 'required|date',
        ]);
        $figura->saveOrFail();
        //return redirect('figuras');
        return redirect()->route('figuras.index')->with('success','Figura creada');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $figura = Figuras::find($id);
        $marcas = Marcas::all();
        return view('editFigura',compact('figura','marcas'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $figura = Figuras::find($id);
        $request->validate([
            'nombre' => 'required|string|max:100',
            'origen' => 'required|string|max:50',
            'id_marca' => 'required|numeric',
            'categoria' => 'required|string|max:50',
            'escala' => 'required|string',
            'precio' => 'required|numeric|digits_between:1,10',
            'fecha' => 'required|date',
        ]);
        $figura->fill($request->input())->saveOrFail();
        return redirect()->route('figuras.index')->with('success','Figura actualizada');
        //return redirect('figuras');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $figura = Figuras::find($id);
        $figura->delete();
        //return redirect('figuras');
        return redirect()->route('figuras.index')->with('success','Figura eliminada');
    }
    
}
