<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Marcas;

class MarcasController extends Controller
{
 
    public function index()
    {
        $marcas = Marcas::all();
        return view('marcas',compact('marcas'));
    }

  
    public function create()
    {
        //
    }

 
    public function store(Request $request)
    {
        $marca = new Marcas($request->input());
        $request->validate([
            'marca' => 'required|string|max:50|unique:marcas',
        ]);
        $marca->saveOrFail();
        //return redirect('marcas');
        return redirect()->route('marcas.index')->with('success','Marca creada');
    }

    public function show(string $id)
    {
        $marca = Marcas::find($id);
        return view('editMarca',compact('marca'));
    }


    public function edit(string $id)
    {
        //
    }


    public function update(Request $request, string $id)
    {
        $marca = Marcas::find($id);
        $request->validate([
            'marca' => 'required|string|max:50|unique:marcas,marca,'.$id
        ]);
        $marca->fill($request->input())->saveOrFail();
        return redirect()->route('marcas.index')->with('success','Marca actualizada');
        //return redirect('marcas');
    }


    public function destroy(string $id)
{
    try {
        $marca = Marcas::findOrFail($id);
        $marca->delete();
        return redirect()->route('marcas.index')->with('success','Marca eliminada');
    } catch (\Illuminate\Database\QueryException $e) {
        // manejar el error, por ejemplo redirigir con un mensaje de error
        return redirect()->route('marcas.index')->with('error','No se puede eliminar una marca que está siendo utilizada por una figura');
    }
}
}
