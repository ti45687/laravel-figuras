<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Figuras extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['nombre','origen','categoria','escala','precio','fecha','id_marca'];
}
