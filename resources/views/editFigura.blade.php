@extends('plantilla')
@section('contenido')
@if ($errors->any())
    <div class="row mt-3">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-danger alert-dismissible fade show">
                <ul>
                    @foreach ($errors->all() as $e)
                        <li>{{$e}}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
@endif
<div class="row mt-3">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header bg-dark text-white">
                Editar figura   
            </div>
            <div class="card-body">
                <form id="frmMarcas" method="POST" action="{{url("figuras",[$figura])}}">
                    @csrf
                    @method('PUT')
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-user"></i></span>
                        <input type="text" name="nombre" value="{{ $figura->nombre}}" class="form-control" maxlength="50" placeholder="Nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <input type="text" name="origen" value="{{ $figura->origen}}" class="form-control" maxlength="50" placeholder="Origen" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                        <select name="id_marca" class="form-select" required>
                            <option value="">Marca</option>
                            @foreach($marcas as $row)
                                @if ($row->id == $figura->id_marca)
                                    <option selected value="{{ $row->id }}">{{ $row->marca }}</option>
                                @else
                                    <option value="{{$row->id}}">{{ $row->marca }}</option>
                                @endif    
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-tag"></i></span>
                        <input type="text" value="{{ $figura->categoria}}" name="categoria" class="form-control" maxlength="50" placeholder="Categoria" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-arrow-up-wide-short"></i></span>
                        <select name="escala" class="form-select" required>
                            <option value="">Escala</option>
                            @foreach(['1/1', '1/4', '1/5', '1/6', '1/7', '1/8', '1/9', '1/12', '1/20', 'Sin escala'] as $escalaOption)
                                @if ($escalaOption == $figura->escala)
                                    <option selected value="{{ $escalaOption }}">{{ $escalaOption }}</option>
                                @else
                                    <option value="{{ $escalaOption }}">{{ $escalaOption }}</option>
                                @endif
                            @endforeach
                        </select> 
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-money-bill"></i></span>
                        <input type="number" name="precio" value="{{ $figura->precio}}" class="form-control" maxlength="50" placeholder="Precio" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-calendar"></i></span>
                        <input type="date" name="fecha" class="form-control" value="{{ $figura->fecha}}" maxlength="50" placeholder="Fecha" required>
                    </div>
                    
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection