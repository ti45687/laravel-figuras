@extends('plantilla')
@section('contenido')
@if ($mensaje = Session::get('success'))
   <div class="row mt-3" id="alertadiv">
    <div class="col-md-6 offset-md-3">
        <div class="alert alert-success  fade show">
            <i class="fa-solid fa-check"></i> {{ $mensaje }}
        </div>
    </div>
   </div>
@endif
@if ($errors->any())
    <div class="row mt-3 " id="alertadiv">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-danger alert-dismissible fade show">
                <ul>
                    @foreach ($errors->all() as $e)
                        <li>{{$e}}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
@endif
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <button class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalMarcas">
                    <i class="fa-solid fa-circle-plus"></i> Añadir
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-10 offset-1">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NOMBRE</th>
                            <th>ORIGEN</th>
                            <th>MARCA</th>
                            <th>CATEGORIA</th>
                            <th>ESCALA</th>
                            <th>PRECIO</th>
                            <th>FECHA</th>
                            <th>EDITAR</th>
                            <th>ELIMINAR</th>
                        </tr>
                    </thead>
                    <tbody class="table-group-divider">
                        @php 
                            $i=1;
                        @endphp
                        @foreach ($figuras as $row)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $row->nombre }}</td>
                                <td>{{ $row->origen }}</td>
                                <td>{{ $row->marca }}</td>
                                <td>{{ $row->categoria }}</td>
                                <td>{{ $row->escala }}</td>
                                <td>$ {{ $row->precio }}</td>
                                <td>{{ $row->fecha }}</td>
                                <td>
                                    <a href="{{url('figuras',[$row])}}" class="btn btn-warning"><i class="fa-solid fa-edit"></i></a>
                                </td>
                                <td>
                                    <form method="POST" action="{{url('figuras',[$row]) }}">
                                        @method("delete")
                                        @csrf
                                        <button class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalMarcas" tabindex="-1" aria-hidden="true">        
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="h5" id="titulo_modal">Añadir figura</label>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" arial-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="frmMarcas" method="POST" action="{{url("figuras")}}">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-user"></i></span>
                        <input type="text" name="nombre" class="form-control" maxlength="50" placeholder="Nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <input type="text" name="origen" class="form-control" maxlength="50" placeholder="Origen" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-building"></i></span>
                        <select name="id_marca" class="form-select" required>
                            <option value="">Marca</option>
                            @foreach($marcas as $row)
                            <option value="{{ $row->id }}">{{ $row->marca }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-tag"></i></span>
                        <input type="text" name="categoria" class="form-control" maxlength="50" placeholder="Categoria" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-arrow-up-wide-short"></i></span>
                        <select name="escala" class="form-select" required>
                            <option value="">Escala</option>
                            <option value="1/1">1/1</option>
                            <option value="1/4">1/4</option>
                            <option value="1/5">1/5</option>
                            <option value="1/6">1/6</option>
                            <option value="1/7">1/7</option>
                            <option value="1/8">1/8</option>
                            <option value="1/9">1/9</option>
                            <option value="1/12">1/12</option>
                            <option value="1/20">1/20</option>
                            <option value="Sin escala">Sin escala</option>
                        </select> 
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-money-bill"></i></span>
                        <input type="number" name="precio" maxlength="10" class="form-control"  placeholder="Precio en mxn" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-calendar"></i></span>
                        <input type="date" name="fecha" class="form-control" maxlength="50" placeholder="Fecha" required>
                    </div>
                    
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"> <i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>  
            </div>
        </div>
    </div>
@endsection
@section('js')
@vite('resources/js/listado.js')        
@endsection
