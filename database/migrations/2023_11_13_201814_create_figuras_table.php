<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('figuras', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100);
            $table->string('origen',100);
            $table->string('categoria',50);
            $table->string('escala',10);
            $table->integer('precio');
            $table->date('fecha',6);
            $table->foreignId('id_marca')->constrained('marcas')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('figuras');
    }
};
